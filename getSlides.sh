#! /bin/bash
# contact: yannick.parmentier@loria.fr
# date: 2019/07/08

name=$1

if [ $# -lt 1 ];
then
	echo "Usage :"
	echo "        $0 <file_name>"
	exit 1
else
	cp .slides-template.html $name
	echo "Slides file created."
fi
exit 0

